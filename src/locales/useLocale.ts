import { useState } from "react";

import donateEn from "./en-US/donate.json";

const resources = {
  "en-US": {
    donate: donateEn,
  },
}

type LocaleResources = typeof resources;
export type Locale = keyof LocaleResources;
type LocaleScope = keyof LocaleResources["en-US"];
export type LangNameSpace = keyof LocaleResources["en-US"]["donate"];

export const useLocale = (scope: LocaleScope = "donate") => {
  const [locale, setLocale] = useState<Locale>("en-US");

  const t = (key: LangNameSpace) => {
    return resources[locale][scope][key];
  }

  return {
    t,
    locale,
    setLocale,
  }
}
