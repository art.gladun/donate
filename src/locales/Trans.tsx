import { Fragment, createElement } from "react";
import { LangNameSpace, useLocale } from "./useLocale";

interface TransProps {
  tKey: LangNameSpace;
  values?: Record<string, string | number>;
  components?: Record<string, React.ReactNode>;
}

// note() I know I could use i18next library and this solution is not perfect
// however I wanted to try implement something similiary my own :)

const getJsxChildren = (text: string, components: Record<string, any>) => {
  const children: any = [];

  const r = new RegExp('<(.*?)>', 'g');
  const iterator = text.matchAll(r);
  let next = iterator.next();
  let lastIndex = 0;

  while (!next.done) {
    const { value } = next;
    const tagName = value[1];
    const tag = value[0];
    const index = value.index as number;

    const slicedText = text.slice(lastIndex, index);
    if (slicedText) {
      children.push(createElement(Fragment, { key: children.length }, slicedText));
    }

    lastIndex = index + value[0].length;
    const openingTag = tagName[0] === '/' ? tagName.slice(1, tagName.length) : tagName;

    // TODO self closing tasgs <Tag />
    if (tagName[0] === '/' && components[openingTag]) {
      const nestedChildren: any = [];
      let lastChild = children.pop();
      let key = 0;

      while (lastChild !== `<${openingTag}>` && lastChild) {
        nestedChildren.unshift(createElement(Fragment, { key }, lastChild));
        lastChild = children.pop();
        key++;
      }

      if (!lastChild) {
        children.push(nestedChildren.map((child: any, i: number) => createElement(Fragment, { key: i }, child)))
      } else {
        children.push(createElement(components[openingTag].type, { ...components[openingTag].props, key: children.length }, nestedChildren));
      }

    } else {
      children.push(tag);
    }

    next = iterator.next();
  }

  const slicedText = text.slice(lastIndex, text.length);
  if (slicedText) {
    children.push(createElement(Fragment, { key: children.length }, slicedText));
  }

  return children;
}

export const Trans: React.FC<TransProps> = ({ tKey, values, components = {} }) => {
  const { t } = useLocale();
  const translation = t(tKey);

  const text = Object.entries(values || {}).reduce((acc, [key, val]) => {
    const r = new RegExp("{{" + key + "}}", 'g');
    return acc.replaceAll(r, `${val}`);
  }, translation);

  const children = getJsxChildren(text, components);
  return <>{children}</>
}
