import { format as formatFNS } from "date-fns";
import { enUS, Locale as LocaleFNS } from "date-fns/locale";

import { useLocale, Locale } from "./useLocale";

export const API_DATE_FORMAT = 'yyyy-MM-dd';

const localeDict: Record<Locale, LocaleFNS> = {
  'en-US': enUS,
}

export const useDates = () => {
  const { locale } = useLocale();

  const localizedFormat: typeof formatFNS = (date, formatString, options) => {
    return formatFNS(date, formatString, { locale: localeDict[locale], ...options })
  }

  return {
    localizedFormat,
    format: formatFNS,
  }
}
