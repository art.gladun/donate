import { useState } from "react";
import { useLocale } from "./useLocale";

export type Currency = 'USD';

export const useNumbers = () => {
  const { locale } = useLocale();
  const [currency, setCurrency] = useState<Currency>("USD");

  const moneyFormatter = new Intl.NumberFormat(locale, {
    style: 'currency',
    currency,
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });

  const formatter = new Intl.NumberFormat(locale, {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });

  const formatMoney = (amount: number) => {
    return moneyFormatter.format(amount);
  }

  const formatNumber = (amount: number) => {
    return formatter.format(amount);
  }

  return {
    formatMoney,
    formatNumber,

    currency,
    setCurrency,
  }
}
