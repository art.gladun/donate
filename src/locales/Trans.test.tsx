import React from 'react';
import { render, screen } from '@testing-library/react';
import { Trans } from './Trans';
import { useLocale } from './useLocale';

jest.mock('./useLocale');

describe('MonthPicker', () => {
  test('should return translation if no other props provided', () => {
    // Arrange
    const translation = 'Translation';
    (useLocale as jest.Mock).mockImplementation(() => ({ t: () => translation }));

    // Act
    render(<Trans tKey='CONTINUE' />);

    // Arrange
    expect(screen.getByText(/Translation/)).toBeInTheDocument();
  });

  test('should replace placeholders with values', () => {
    // Arrange
    const translation = 'Try to replace {{here}} and {{here}} and {{there}}';
    (useLocale as jest.Mock).mockImplementation(() => ({ t: () => translation }));
    const values = { here: 'here', there: 'there' };

    // Act
    render(<Trans tKey='CONTINUE' values={values} />);

    // Arrange
    expect(screen.getByText(/Try to replace here and here and there/)).toBeInTheDocument();
  });

  test('should replace components placeholders with react components and all their props', () => {
    // Arrange
    const translation = 'Try to replace <b>here</b> and <date>here</date> and there';
    (useLocale as jest.Mock).mockImplementation(() => ({ t: () => translation }));

    const Date: React.FC<{ format: string } & React.PropsWithChildren> = ({ format, children }) =>
      <span data-format={format} data-testid="date">{children}</span>
    const components = { date: <Date format="YYYY" /> };

    // Act
    render(<Trans tKey='CONTINUE' components={components} />);

    // Arrange
    const dateC = screen.getByTestId("date");
    expect(dateC).toBeInTheDocument();
    expect(dateC).toHaveAttribute("data-format");
  });

  test('shouldn try to resolve component even if the given translation contains wrong html structure', async () => {
    // Arrange
    const translation = 'Try to replace <b>here and <date>here</b></date> and there';
    (useLocale as jest.Mock).mockImplementation(() => ({ t: () => translation }));

    const Date: React.FC<{ format: string } & React.PropsWithChildren> = ({ format, children }) =>
      <span data-format={format} data-testid="date">{children}</span>
    const components = { date: <Date format="YYYY" /> };

    // Act
    render(<Trans tKey='CONTINUE' components={components} />);

    // Arrange
    const dateC = screen.getByTestId("date");
    expect(dateC).toBeInTheDocument();
  });
});
