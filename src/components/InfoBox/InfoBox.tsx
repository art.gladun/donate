import { PropsWithChildren } from "react";

interface InfoBoxProps extends PropsWithChildren {}

export const InfoBox: React.FC<InfoBoxProps> = ({ children }) => {
  return <div className="bg-blueGray-25 text-xs rounded-md p-4 text-blueGray-900" data-testid="infoBox" tabIndex={0}>
    {children}
  </div>
}
