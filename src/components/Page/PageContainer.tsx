import classnames from "classnames";
import React from "react";

import { PropsWithAs } from "utils";

interface Props {
  className?: string;
}

export const PageContainer: React.FC<PropsWithAs<Props, "div" | "nav">> = ({
  children,
  as: Component = "div",
  className,
}) => {
  const classes = classnames(className, "max-w-5xl mx-auto p-6");
  return <Component className={classes}>{children}</Component>;
};
