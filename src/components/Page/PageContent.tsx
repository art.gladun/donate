import React from "react";
import classNames from "classnames";

interface Props {
  className?: string;
}

export const PageContent: React.FC<React.PropsWithChildren<Props>> = ({
  children,
  className,
}) => {
  const styles = classNames("p-4", className);
  return <div className={styles}>{children}</div>;
};
