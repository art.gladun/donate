import { addMonths, differenceInCalendarMonths } from "date-fns";

import { Button } from "components/Button";
import { FormInputWraper } from "components/FormGroup";
import { ChevronLeftIcon, ChevronRightIcon } from "components/Icons";

import { useDates, useLocale } from "locales";

interface MonthPickerProps {
  value: Date;
  onChange?: (value: Date) => void;
  onBlur?: () => void;
  min?: Date;
}

export const MonthPicker: React.FC<MonthPickerProps> = ({
  value,
  onChange,
  onBlur,
  min,
  ...rest
}) => {
  const { t } = useLocale();
  const { localizedFormat } = useDates();

  const prevMonth = addMonths(value, -1);
  const nextMonth = addMonths(value, 1);

  const isPrevDisabled = Boolean(min && differenceInCalendarMonths(prevMonth, min) === -1);

  const handlePrevClick = () => {
    if (onChange && !isPrevDisabled) {
      onChange(prevMonth);
    }
  };

  const handleNextClick = () => {
    if (onChange) {
      onChange(nextMonth);
    }
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'ArrowLeft' || event.key === 'ArrowDown') {
      handlePrevClick();
    }

    if (event.key === 'ArrowRight' || event.key === 'ArrowUp') {
      handleNextClick()
    }
  }

  return (
    <FormInputWraper className="flex gap-4 p-3" onKeyDown={handleKeyDown} data-testid="monthPicker">
      <input className="sr-only" readOnly aria-readonly type="text" value={localizedFormat(value, "MMMM yyyy")} {...rest} />
      <div className="flex items-center">
        <Button
          as="button"
          type="button"
          size="xs"
          variant="ghost"
          onClick={handlePrevClick}
          onBlur={onBlur}
          disabled={isPrevDisabled}
          data-testid="prevMonth"
          aria-label={t("PREV_MONTH")}
        >
          <ChevronLeftIcon />
        </Button>
      </div>
      <div
        className="flex flex-col flex-grow text-center"
        onBlur={onBlur}
      >
        <span className="font-['Rubik'] font-medium leading-[20px] text-blueGray-700">
          <span>{localizedFormat(value, "MMMM")}</span>
        </span>
        <span className="text-xs font-['Work_Sans'] leading-[16px] text-blueGray-700">
          {localizedFormat(value, "yyyy")}
        </span>
      </div>
      <div className="flex items-center">
        <Button
          as="button"
          type="button"
          size="xs"
          variant="ghost"
          onClick={handleNextClick}
          onBlur={onBlur}
          data-testid="nextMonth"
          aria-label={t("NEXT_MONTH")}
        >
          <ChevronRightIcon />
        </Button>
      </div>
    </FormInputWraper>
  );
};
