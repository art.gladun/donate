import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { MonthPicker } from './MonthPicker';

describe('MonthPicker', () => {
  test('should render given month and year', () => {
    // Arrange
    const date = new Date('2024-01-20');

    // Act
    render(<MonthPicker value={date}/>);

    // Assert
    expect(screen.getByText(/January/)).toBeInTheDocument();
    expect(screen.getByText(/2024/)).toBeInTheDocument();
  });

  test('should invoke the onChange callback when prev and next month buttons are clicked', () => {
    // Arrange
    const date = new Date('2024-01-20');
    const handleChangeFn = jest.fn();

    // Act
    render(<MonthPicker value={date} onChange={handleChangeFn} />);

    screen.getByTestId('prevMonth').click();
    screen.getByTestId('nextMonth').click();

    // Assert
    const calls = handleChangeFn.mock.calls;
    const prevMonth = new Date(calls[0]).getMonth();
    const nextMonth = new Date(calls[1]).getMonth();

    expect(prevMonth).toBe(11);
    expect(nextMonth).toBe(1);
  });

  test('should invoke the onChange callback when keyboard arrows are pushed', () => {
    // Arrange
    const date = new Date('2024-01-20');
    const handleChangeFn = jest.fn();

    // Act
    render(<MonthPicker value={date} onChange={handleChangeFn} />);

    const monthPicker = screen.getByTestId('monthPicker');

    fireEvent.keyDown(monthPicker, { key: 'ArrowLeft' });
    fireEvent.keyDown(monthPicker, { key: 'ArrowRight' });

    // Assert
    const calls = handleChangeFn.mock.calls;
    const prevMonth = new Date(calls[0]).getMonth();
    const nextMonth = new Date(calls[1]).getMonth();

    expect(prevMonth).toBe(11);
    expect(nextMonth).toBe(1);
  });

  test('shouldnt invoke the onChange callback when given date is equal to min', () => {
    // Arrange
    const date = new Date('2024-01-20');
    const handleChangeFn = jest.fn();

    // Act
    render(<MonthPicker value={date} onChange={handleChangeFn} min={date} />);

    const prevMonthButton = screen.getByTestId('prevMonth')
    prevMonthButton.click();

    // Assert
    expect(handleChangeFn).not.toHaveBeenCalled();
    expect(prevMonthButton).toBeDisabled();
  });
});

