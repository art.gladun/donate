import { forwardRefWithAs, PropsWithAs } from "utils";

export interface TextComponentProps {
  as?: React.ElementType;
}

// TODO it would be really nice to define fontSize with lineHeight here!
export const TextComponent = (
  props: PropsWithAs<TextComponentProps, "span">,
  forwardedRef: React.Ref<HTMLSpanElement>
) => {
  const {
    as: Component = "span",
    children,
    color,
    ...rest
  } = props;

  return (
    <Component ref={forwardedRef} {...rest}>
      {children}
    </Component>
  );
};

export type TextProps = PropsWithAs<TextComponentProps, "span">;
export const Text = forwardRefWithAs<TextComponentProps, "span">(TextComponent);
