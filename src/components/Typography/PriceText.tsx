import { useNumbers } from "locales";
import { Text } from "./Text";

interface PriceProps {
  price: number;
}

export const PriceText: React.FC<PriceProps> = ({ price }) => {
  const { formatMoney } = useNumbers();

  return <Text className="font-bold">{formatMoney(price)}</Text>
}
