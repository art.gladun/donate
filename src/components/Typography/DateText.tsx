import { useDates } from "locales";

import { Text } from "./Text";

interface DateProps {
  date: Date;
  format: string;
}

export const DateText: React.FC<DateProps> = ({ date, format }) => {
  const { localizedFormat } = useDates();

  return <Text className="font-bold">{localizedFormat(date, format)}</Text>
}
