import classNames from "classnames";
import { FormInputWraper } from "components/FormGroup"
import { PropsWithAs, forwardRefWithAs } from "utils";

interface NumberInputComponentProps {
  leftPlaceholder?: React.ReactNode;
}

export const NumberInputComponent = (
  props: PropsWithAs<NumberInputComponentProps, "input">,
  forwardRef: React.Ref<HTMLInputElement>,
) => {
  const { value, leftPlaceholder, ...rest } = props;

  const isLeftPlaceholder = typeof leftPlaceholder !== 'undefined';

  const wrapperStyle = classNames(
    "inline-block py-4 relative w-full",
    {
      "px-4": !isLeftPlaceholder,
      "px-2": isLeftPlaceholder,
    }
  );

  const placeholderStyle = classNames(
    "absolute top-0 h-full flex items-center text-blueGray-600 text-2xl",
  );

  const inputStyle = classNames(
    "h-[28px] text-xl font-medium font-['Rubik'] text-blueGray-700 outline-none w-full placeholder:text-[#bebebe]",
    {
      "pl-7": isLeftPlaceholder,
    }
  )

  return <FormInputWraper className={wrapperStyle}>
    <div className={placeholderStyle}>
      {leftPlaceholder}
    </div>
    <input
      className={inputStyle}
      type="number"
      inputMode="numeric"
      pattern="[0-9]*"
      value={value}
      ref={forwardRef}
      {...rest} />
  </FormInputWraper>
}

export type NumberInputProps = PropsWithAs<NumberInputComponentProps, "input">;
export const NumberInput = forwardRefWithAs<NumberInputComponentProps, "input">(
  NumberInputComponent,
);
