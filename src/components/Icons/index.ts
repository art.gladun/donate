import { createIcon } from "./createIcon";

import { ReactComponent as ChevronLeftSvg } from "./svg/chevron-left.svg";
import { ReactComponent as ChevronRightSvg } from "./svg/chevron-right.svg";
import { ReactComponent as CloseSvg } from "./svg/close.svg";
import { ReactComponent as DolarSvg } from "./svg/dolar.svg";
import { ReactComponent as FamilySvg } from "./svg/family.svg";

export const ChevronLeftIcon = createIcon(ChevronLeftSvg);
export const ChevronRightIcon = createIcon(ChevronRightSvg);
export const CloseIcon = createIcon(CloseSvg);
export const DolarIcon = createIcon(DolarSvg);
export const FamilyIcon = createIcon(FamilySvg);
