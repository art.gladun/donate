import React, {
  ComponentType,
  forwardRef,
  SVGAttributes,
  SVGProps,
} from "react";
import classNames from "classnames";

export interface ISvgIconProps extends SVGAttributes<SVGSVGElement> {
  // TODO consider presets possible colors or sizes
  // the implementation now iherit the color and size from the parent
}

export interface ISvgIconInternalProps extends ISvgIconProps {
  Svg: React.ComponentType<SVGProps<SVGSVGElement>>;
}

const useClassNames = (props: ISvgIconProps) => {
  return classNames(
    "select-none",
    "shrink-0",
    "fill-current",
    "h-[1em]",
    "w-[1em]",
    props.className,
  );
};

export const SvgIcon = forwardRef<SVGSVGElement, ISvgIconInternalProps>(
  (props, ref) => {
    const { Svg, ...restProps } = props;
    const classNames = useClassNames(restProps);

    return (
      <Svg
        focusable="false"
        aria-hidden="true"
        data-icon
        className={classNames}
        {...restProps}
        ref={ref}
      />
    );
  },
);

export type TSvgIconProps = React.ComponentPropsWithRef<typeof SvgIcon>;

export const createIcon = (Svg: ComponentType<SVGProps<SVGSVGElement>>) => {
  return forwardRef<SVGSVGElement, ISvgIconProps>((props, ref) => (
    <SvgIcon Svg={Svg} {...props} ref={ref} />
  ));
};
