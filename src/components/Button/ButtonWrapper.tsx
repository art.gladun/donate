import classNames from "classnames";
import { PropsWithChildren } from "react";

interface ButtonWrapperProps extends PropsWithChildren {
  className?: string;
}

export const ButtonWrapper: React.FC<ButtonWrapperProps> = ({ children, className }) => {
  const styles = classNames("p-2", className);
  return <div className={styles}>{children}</div>
}
