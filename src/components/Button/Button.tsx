import classNames from "classnames";

import { PropsWithAs, forwardRefWithAs } from "utils/withAs";

const variantPrimary = classNames(
  "bg-blueGray-800",
  "border-blueGray-800",

  "enabled:hover:bg-[#645D93]",
  "enabled:hover:border-[#645D93]",

  "enabled:focus:bg-[#645D93]",
  "enabled:focus:border-[#645D93]",

  "enabled:active:bg-[#241E47]",
  "enabled:active:border-[#241E47]",

  "text-white",
);

const variantSecondary = classNames(
  "bg-white",
  "border-blueGray-700",

  "enabled:hover:bg-[#f7f6fe]",
  "enabled:hover:border-blueGray-700",

  "enabled:focus:bg-[#f7f6fe]",
  "enabled:focus:border-blueGray-700",

  "enabled:active:bg-[#ece9fc]",
  "enabled:active:border-[#241E47]",

  "text-blueGray-700",
);

const variantGhost = classNames(
  "bg-transparent",
  "border-transparent",

  "enabled:hover:bg-[#F3F5FE]",
  "enabled:hover:border-#F3F5FE",

  "enabled:focus:bg-[#F3F5FE]",
  "enabled:focus:border-#F3F5FE",

  "enabled:active:bg-[#E8EAF2]",
  "enabled:active:border-[#E8EAF2]",

  "text-blueGray-600",
);

const salmonGhost = classNames(
  "bg-transparent",
  "border-transparent",

  "enabled:hover:bg-[#F2D0C1]",
  "enabled:hover:border-#F2D0C1",

  "enabled:focus:bg-[#F2D0C1]",
  "enabled:focus:border-#F2D0C1",

  "enabled:active:bg-[#F2D0C1]",  // something darker??
  "enabled:active:border-[#F2D0C1]",

  "text-blueGray-700",
);

const baseStyles = classNames("border  rounded-md font-semibold font-['Work_Sans']");

export type ButtonVariatn = "primary" | "secondary" | "ghost" | "salmon-ghost";
export type ButtonDisplay = "inline-block" | "block";
export type ButtonSize = "md" | "sm" | "xs";

const variantDict: Record<ButtonVariatn, string> = {
  primary: variantPrimary,
  secondary: variantSecondary,
  ghost: variantGhost,
  "salmon-ghost": salmonGhost,
};

const displayStyles: Record<ButtonDisplay, string> = {
  block: "block w-full",
  "inline-block": "inline-block",
};

const sizeStyles: Record<ButtonSize, string> = {
  "md": "px-5 py-3",
  "sm": "p-2",
  "xs": "p-1",
}

interface ButtonComponentProps {
  variant?: ButtonVariatn;
  display?: ButtonDisplay;
  size?: ButtonSize;
}

export const ButtonComponent = (
  props: PropsWithAs<ButtonComponentProps, "button">,
  forwardRef: React.Ref<HTMLButtonElement>,
) => {
  const {
    as: Component = "button",
    variant = "primary",
    className,
    disabled,
    children,
    size = "md",
    display = "inline-block",
    ...rest
  } = props;

  const classes = classNames(
    baseStyles,
    variantDict[variant],
    displayStyles[display],
    sizeStyles[size],
    className,
    {
      'opacity-60': disabled,
    }
  );

  return (
    <Component
      disabled={disabled}
      aria-disabled={Boolean(disabled)}
      className={classes}
      ref={forwardRef} {...rest}>
      {children}
    </Component>
  );
};

export type ButtonProps = PropsWithAs<ButtonComponentProps, "button" | "a">;
export const Button = forwardRefWithAs<ButtonComponentProps, "button" | "a">(
  ButtonComponent,
);
