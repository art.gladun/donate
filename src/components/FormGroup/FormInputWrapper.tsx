import classNames from "classnames";
import { PropsWithChildren } from "react";

type TDivProps = JSX.IntrinsicElements["div"];

interface FormInputWraperProps extends PropsWithChildren, TDivProps {
  className?: string;
}

export const FormInputWraper: React.FC<FormInputWraperProps> = ({ children, className, ...rest }) => {
  const style = classNames("has-[:focus]:border-blueGray-800 border border-blueGray-50 rounded-md", className);
  return <div className={style} {...rest}>
    {children}
  </div>
}
