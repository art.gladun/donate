import { useMemo } from "react";

import { generateId } from "utils/id";

interface FormGroupAriaProps {
  useInput?: boolean;
  useLabel?: boolean;
  useError?: boolean;
  baseId?: string;
}

type LabelAria = {
  htmlFor?: string;
  id?: string;
};

type ErrorAria = {
  role: string;
  id: string;
};

type InputAria = {
  "aria-labelledby"?: string;
  "aria-describedby"?: string;
  id?: string;
};

interface FormGroupAriaReturn {
  inputAria?: InputAria;
  labelAria?: LabelAria;
  errorAria?: ErrorAria;
}

export const useFormGroupAria = (props: FormGroupAriaProps = {}) => {
  const { useLabel, useInput, useError, baseId } = props;

  return useMemo(() => {
    const inputId = generateId(baseId);
    const labelId = generateId(baseId);

    const returnState: FormGroupAriaReturn = {};

    if (useError) {
      returnState["errorAria"] = {
        id: generateId(baseId),
        role: "alert",
      };
    }

    if (useInput) {
      returnState["inputAria"] = {
        id: inputId,
        "aria-labelledby": useLabel ? labelId : undefined,
        // aria-describedby for assistive elements
      };
    }

    if (useLabel) {
      returnState["labelAria"] = {
        id: labelId,
        htmlFor: useInput ? inputId : undefined,
      };
    }

    return returnState;
  }, [useLabel, useInput, useError, baseId]);
};
