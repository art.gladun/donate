import { PropsWithChildren } from "react";

type TLabelProps = JSX.IntrinsicElements["label"];

interface Props extends TLabelProps, PropsWithChildren {}

export const FormLabel: React.FC<Props> = ({ children, ...rest }) => {
  return (
    <label className="text-sm leading-[18px] font-medium font-['Work_Sans'] text-blueGray-600" {...rest}>
      {children}
    </label>
  );
};
