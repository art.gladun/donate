import { PropsWithChildren } from "react";
import classNames from "classnames";

export interface Props extends PropsWithChildren {
    label?: React.ReactNode;
    className?: string;
  }

  export const FormGroup: React.FC<Props> = ({ children, label, className }) => {
    const classes = classNames(className, "flex flex-col space-y-1 flex-start")

    return (
      <div className={classes}>
        {label}
        <div>{children}</div>
        {/* Place for errors and assistive texts */}
      </div>
    );
  };
