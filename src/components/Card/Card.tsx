import classNames from "classnames";

import { PropsWithChildren } from "react";

interface CardProps extends PropsWithChildren {
  className?: string;
}

export const Card: React.FC<CardProps> = ({ children, className }) => {
  const styles = classNames("max-w-[600px] rounded-md shadow-card shadow-blueGray-900/10", className);

  return <div className={styles}>
    {children}
  </div>
}
