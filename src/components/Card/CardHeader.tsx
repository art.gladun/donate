import { Button } from "components/Button";
import { CloseIcon } from "components/Icons";
import { useLocale } from "locales";

interface CardHeaderProps {
  icon: React.ReactNode;
  headerPlaceholder: React.ReactNode;
  subheaderPlaceholder: React.ReactNode;
  onClose?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  closeButtonAriaLabel?: string;
}

export const CardHeader: React.FC<CardHeaderProps> = ({ icon, headerPlaceholder, subheaderPlaceholder, onClose, closeButtonAriaLabel }) => {
  const { t } = useLocale();

  return <div className="bg-salmon-400 p-6 md:p-8 rounded-t-md md:rounded-t-lg relative">
    <div className="flex flex-col md:flex-row gap-4">
      <div className="flex justify-center text-7xl text-blueGray-800">
        {icon}
      </div>
      <div className="flex flex-col md:gap-2" tabIndex={0}>
        <div className="flex justify-center md:justify-normal">{headerPlaceholder}</div>
        <div className="flex justify-center md:justify-normal">{subheaderPlaceholder}</div>
      </div>
    </div>
    {onClose && <div className="absolute top-0 right-0 p-2 md:hidden">
      <Button type="button" size="sm" variant="salmon-ghost" aria-label={closeButtonAriaLabel || t("CLOSE_BUTTON_ARIA_LABEL")}><CloseIcon /></Button>
    </div>}
  </div>
}
