import { Text, TextProps } from "components/Typography";

interface Props extends TextProps {}

export const CardHeading: React.FC<Props> = ({ children }) => {
  return <Text as="h1" className="font-['Work_Sans'] font-semibold text-[24px] md:text-[32px] leading-[120%] text-blueGray-800">{children}</Text>
}

interface Props extends TextProps {}

export const CardSubHeading: React.FC<Props> = ({ children }) => {
  return <Text as="h2" className="font-['Inter'] text-[16px] leading-[20px] text-blueGray-700">{children}</Text>
}
