import { PropsWithChildren } from "react";

export const CardContent: React.FC<PropsWithChildren> = ({ children }) => {
  return <div className="bg-white p-6 md:p-8 rounded-b-md md:rounded-b-lg">
    {children}
  </div>
}
