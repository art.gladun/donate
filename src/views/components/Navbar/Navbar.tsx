import { PageContainer } from "components/Page";

import { NavLogo } from "./NavLogo";

export const Navbar = () => {
  return (
    <div className="bg-white">
      <PageContainer as="nav">
        <NavLogo />
      </PageContainer>
    </div>
  );
};
