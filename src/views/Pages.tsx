import React from "react";
import { Route, Routes, useNavigate } from "react-router-dom";

import { PageLayout } from "components/Page";

import { DonatePage } from "views/Donate";
import { Navbar } from "views/components/Navbar";

import { PATHS } from "./paths";
import { useEffect } from "react";

const Redirect = () => {
  const navigate = useNavigate();

  useEffect(() => {
    navigate(PATHS.DONATE.ROOT.toPath());
  }, [navigate]);

  return null;
};

export const Pages = () => {
  return (
    <PageLayout>
      <Navbar />
      <Routes>
        <Route path={PATHS.DONATE.ROOT.PATH} element={<DonatePage />} />
        <Route path="*" element={<Redirect />} />
      </Routes>
    </PageLayout>
  );
};
