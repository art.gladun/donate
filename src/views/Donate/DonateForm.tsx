import { differenceInCalendarMonths } from "date-fns";
import { useForm, Controller } from "react-hook-form";

import { Button, ButtonWrapper } from "components/Button";
import { InfoBox } from "components/InfoBox";
import { FormGroup, FormLabel, useFormGroupAria } from "components/FormGroup";
import { DolarIcon } from "components/Icons";
import { DateText, PriceText, Text } from "components/Typography";
import { NumberInput } from "components/NumberInput";
import { MonthPicker } from "components/MonthPicker";

import { useLocale, Trans, useNumbers } from "locales";

import { SummaryBox } from "./SummaryBox";

interface IDonateFormValues {
  price: string;
  date: Date;
}

const ALLOWED_PRICE_KEYS = new Set([
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  ",",
  ".",
  "Backspace",
  "Delete",
  "Shift",
  "Tab",
  "ArrowUp",
  "ArrowDown",
  "ArrowLeft",
  "ArrowRight",
]);

const DEFAULT_VALUES = {
  price: '5',
  date: new Date(),
}

export const DonateForm = () => {
  const { t } = useLocale();
  const { formatNumber } = useNumbers();

  const initialDate = DEFAULT_VALUES.date;

  const { register, formState, watch, handleSubmit, control } = useForm<IDonateFormValues>({
    mode: "all",
    defaultValues: DEFAULT_VALUES,
  });

  const priceAriaProps = useFormGroupAria({
    useInput: true,
    useLabel: true,
  });

  const dateAriaProps = useFormGroupAria({
    useInput: true,
    useLabel: true,
  });

  const submitForm = (formValues: IDonateFormValues) => {
    console.log('submit', formValues)
  }

  const onPriceKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (!ALLOWED_PRICE_KEYS.has(event.key)) {
      event.preventDefault();
    }
  };

  const price = Number(watch("price")) || 0;
  const date = watch("date");
  const total = (differenceInCalendarMonths(date, initialDate) + 1) * price;

  return (
    <form className="flex flex-col gap-6 md:gap-8">
      <div className="flex flex-col gap-8 md:gap-2">
        <div className="flex flex-col md:flex-row gap-6 md:gap-8">
          <FormGroup
            className="w-full"
            label={<FormLabel {...priceAriaProps.labelAria}>{t("I_CAN_DONATE_LABEL")}</FormLabel>}
          >
            <NumberInput
              autoFocus
              placeholder={formatNumber(0)}
              leftPlaceholder={<DolarIcon />}
              min={0}
              data-testid="priceInput"
              onKeyDown={onPriceKeyDown}
              {...priceAriaProps.inputAria}
              {...register("price", {
                required: true,
              })}
            />
          </FormGroup>
          <FormGroup
            className="w-full"
            label={<FormLabel {...dateAriaProps.labelAria}>{t("EVERY_MONTH_UNTIL")}</FormLabel>}
          >
            <Controller control={control} name="date" render={({ field: { value, onChange, onBlur }}) => (
              <MonthPicker value={value} onChange={onChange} onBlur={onBlur} min={initialDate} {...dateAriaProps.inputAria} />
            )} />
          </FormGroup>
        </div>
        <div className="flex flex-col border md:border-0 rounded-md border-blueGray-50">
          <SummaryBox total={total} />
          <InfoBox>
            <Trans
              tKey="YOU_WILL_BE_SENDING"
              values={{ date: "August 2023" }}
              components={{
                price: <PriceText price={price} />,
                date: <DateText date={date} format="MMMM yyyy" />,
                text: <Text data-testid="infoText" />,
              }}
            />
          </InfoBox>
        </div>
      </div>
      <ButtonWrapper className="flex gap-8">
        <Button
          type="button"
          variant="secondary"
          display="block"
          className="hidden md:block"
          data-testid="cancel"
        >
          {t("CANCEL")}
        </Button>
        <Button
          as="button"
          type="submit"
          variant="primary"
          display="block"
          onClick={handleSubmit(submitForm)}
          data-testid="submit"
          disabled={!formState.isValid}
        >
          {t("CONTINUE")}
        </Button>
      </ButtonWrapper>
    </form>
  );
};
