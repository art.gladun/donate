import { PriceText } from "components/Typography";

import { useLocale } from "locales";

interface SummaryBoxProps {
  total: number;
}

export const SummaryBox: React.FC<SummaryBoxProps> = ({ total }) => {
  const { t } = useLocale();

  return (
    <div className="flex gap-2 justify-between items-center px-4 py-6" data-testid="summaryBox" tabIndex={0}>
      <div className="text-xl font-medium font-['Work_Sans']">
        {t("TOTAL_AMOUNT")}
      </div>
      <div className="text-3xl font-[Inter] text-blueGray-700">
        <PriceText price={total} />
      </div>
    </div>
  );
};
