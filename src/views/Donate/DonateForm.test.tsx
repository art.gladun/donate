/* eslint-disable testing-library/no-unnecessary-act */
import { format, addMonths } from 'date-fns';
import React from 'react';
import { render, screen, within, fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

import { DonateForm } from './DonateForm';

describe('DonateForm - integration', () => {
  test('should calculate the total ammount based on user actions', async () => {
    const today = new Date(); // equal to initial date;

    act(() => {
      render(<DonateForm />);
    });

    const priceInput = screen.getByTestId('priceInput');
    const prevMonth = screen.getByTestId('prevMonth');
    const nextMonth = screen.getByTestId('nextMonth');
    const summaryBox = screen.getByTestId('summaryBox');
    const infoBox = screen.getByTestId('infoBox');
    const submitButton = screen.getByTestId('submit');

    // initial state
    const { getByText: getByTextInSummaryBox } = within(summaryBox);
    const { getByText: getByTextInInfoBox } = within(infoBox);

    expect(getByTextInSummaryBox('$5.00')).toBeInTheDocument();
    expect(getByTextInInfoBox('$5.00')).toBeInTheDocument();
    expect(getByTextInInfoBox(format(today, "MMMM yyyy"))).toBeInTheDocument();
    expect(prevMonth).toBeDisabled();

    // chose the next month
    act(() => {
      nextMonth.click();
    });
    expect(getByTextInSummaryBox('$10.00')).toBeInTheDocument();
    expect(getByTextInInfoBox('$5.00')).toBeInTheDocument();
    expect(getByTextInInfoBox(format(addMonths(today, 1), "MMMM yyyy"))).toBeInTheDocument();
    expect(prevMonth).not.toBeDisabled();

    // increase the price
    act(() => {
      fireEvent.change(priceInput, {target: { value: '20' }})
    });
    expect(getByTextInSummaryBox('$40.00')).toBeInTheDocument();
    expect(getByTextInInfoBox('$20.00')).toBeInTheDocument();
    expect(getByTextInInfoBox(format(addMonths(today, 1), "MMMM yyyy"))).toBeInTheDocument();

    // clear the input
    act(() => {
      fireEvent.change(priceInput, {target: { value: '' }})
    });

    expect(getByTextInSummaryBox('$0.00')).toBeInTheDocument();
    expect(getByTextInInfoBox('$0.00')).toBeInTheDocument();
    expect(getByTextInInfoBox(format(addMonths(today, 1), "MMMM yyyy"))).toBeInTheDocument();
    expect(submitButton).toBeDisabled();
  });
})
