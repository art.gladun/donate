import { Card, CardHeader, CardContent, CardHeading, CardSubHeading } from "components/Card";
import { FamilyIcon } from "components/Icons";
import { PageContainer } from "components/Page";

import { useLocale } from "locales";

import { DonateForm } from "./DonateForm";

export const DonatePage = () => {
  const { t } = useLocale();

  const handleClose = () => {
    // TODO
  }

  return (
    <PageContainer>
      <Card className="mx-auto">
        <CardHeader
          icon={<FamilyIcon />}
          headerPlaceholder={<CardHeading aria-label={t("THE_GIVING_BLOCK")}>{t("THE_GIVING_BLOCK")}</CardHeading>}
          subheaderPlaceholder={<CardSubHeading>{t("SET_UP_YOUR_DONATION_GOAL")}</CardSubHeading>}
          onClose={handleClose}
        />
        <CardContent>
          <DonateForm />
        </CardContent>
      </Card>
    </PageContainer>
  );
};
