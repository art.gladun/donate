let count = new Date().getTime();

export const generateId = (baseId?: string) => {
  const hash = Number(count++).toString(32);
  return [baseId, hash].filter(Boolean).join("-");
};
