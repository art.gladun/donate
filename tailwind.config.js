/** @type {import('tailwindcss').Config} */

module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      boxShadow: {
        card: '0px 16px 32px 0px'
      },
      colors: {
        blueGray: {
          25: '#F4F8FA',
          50: '#E9EEF2',
          600: '#4D6475',
          700: '#595D7B',
          800: '#423C66',
          900: '#1E2A32',
        },
        salmon: {
          400: '#FFDBCB'
        }
      }
    },
  },
  plugins: [],
}

